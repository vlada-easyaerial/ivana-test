/* eslint-disable no-undef */
const isPointSafe = require("./index");

test("run isPointSafe funtion", () => {
	isPointSafe(
		[
			{
				x: 1,
				y: 1
			},
			{
				x: 13,
				y: 1
			},
			{
				x: 13,
				y: 13
			},
			{
				x: 1,
				y: 13
			}
		],
		[
			[
				{
					x: 3,
					y: 5
				},
				{
					x: 11,
					y: 5
				},
				{
					x: 11,
					y: 9
				},
				{
					x: 3,
					y: 9
				}
			]
		],
		{
			x: 12,
			y: 12
		}
	);
});
