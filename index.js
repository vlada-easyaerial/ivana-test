let INF = 10000;

class Point {
	constructor(x, y) {
		this.x = x;
		this.y = y;
	}
}

// Given three collinear points p, q, r,
// the function checks if point q lies
// on line segment 'pr'
function onSegment(p, q, r) {
	if (
		q.x <= Math.max(p.x, r.x) &&
		q.x >= Math.min(p.x, r.x) &&
		q.y <= Math.max(p.y, r.y) &&
		q.y >= Math.min(p.y, r.y)
	) {
		return true;
	}
	return false;
}

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are collinear
// 1 --> Clockwise
// 2 --> Counterclockwise
function orientation(p, q, r) {
	let val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

	if (val == 0) {
		return 0; // collinear
	}
	return val > 0 ? 1 : 2; // clock or counterclock wise
}

// The function that returns true if
// line segment 'p1q1' and 'p2q2' intersect.
function doIntersect(p1, q1, p2, q2) {
	// Find the four orientations needed for
	// general and special cases
	let o1 = orientation(p1, q1, p2);
	let o2 = orientation(p1, q1, q2);
	let o3 = orientation(p2, q2, p1);
	let o4 = orientation(p2, q2, q1);

	// General case
	if (o1 != o2 && o3 != o4) {
		return true;
	}

	// Special Cases
	// p1, q1 and p2 are collinear and
	// p2 lies on segment p1q1
	if (o1 == 0 && onSegment(p1, p2, q1)) {
		return true;
	}

	// p1, q1 and p2 are collinear and
	// q2 lies on segment p1q1
	if (o2 == 0 && onSegment(p1, q2, q1)) {
		return true;
	}

	// p2, q2 and p1 are collinear and
	// p1 lies on segment p2q2
	if (o3 == 0 && onSegment(p2, p1, q2)) {
		return true;
	}

	// p2, q2 and q1 are collinear and
	// q1 lies on segment p2q2
	if (o4 == 0 && onSegment(p2, q1, q2)) {
		return true;
	}

	// Doesn't fall in any of the above cases
	return false;
}

// Returns true if the point p lies
// inside the polygon[] with n vertices
function isInside(polygon, n, p) {
	// There must be at least 3 vertices in polygon[]
	if (n < 3) {
		return false;
	}

	// Create a point for line segment from p to infinite
	let extreme = new Point(INF, p.y);

	// Count intersections of the above line
	// with sides of polygon
	let count = 0,
		i = 0;
	do {
		let next = (i + 1) % n;

		// Check if the line segment from 'p' to
		// 'extreme' intersects with the line
		// segment from 'polygon[i]' to 'polygon[next]'
		if (doIntersect(polygon[i], polygon[next], p, extreme)) {
			// If the point 'p' is colinear with line
			// segment 'i-next', then check if it lies
			// on segment. If it lies, return true, otherwise false
			if (orientation(polygon[i], p, polygon[next]) == 0) {
				return onSegment(polygon[i], p, polygon[next]);
			}

			count++;
		}
		i = next;
	} while (i != 0);

	// Return true if count is odd, false otherwise
	return count % 2 == 1; // Same as (count%2 == 1)
}

//are two lines intersecting???

function lineLine(x1, y1, x2, y2, x3, y3, x4, y4) {
	// calculate the distance to intersection point
	var uA = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));
	var uB = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / ((y4 - y3) * (x2 - x1) - (x4 - x3) * (y2 - y1));

	// if uA and uB are between 0-1, lines are colliding
	if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {
		return true;
	}
	return false;
}

function pointIsOnLine(x1, y1, x2, y2, x, y) {
	var m;
	if (y2 == y1) {
		if (y == y2) {
			return true;
		} else {
			m = 0;
		}
	} else if (x2 == x1) {
		if (x2 == x) {
			return true;
		} else {
			m = 1;
			if (y2 == y || y1 == y) {
				return true;
			} else {
				return false;
			}
		}
	} else {
		m = (y2 - y1) / (x2 - x1);
	}

	var c = y2 - m * x2;
	// If (x, y) satisfies the equation
	// of the line
	if (y == m * x + c) {
		return true;
	}
	return false;
}

function getLinesForAreaPolygonFromPoints(polygon) {
	let linesArray = [];
	for (let i = 0; i < polygon.length - 1; i++) {
		linesArray.push({
			xa: polygon[i].x,
			ya: polygon[i].y,
			xb: polygon[i + 1].x,
			yb: polygon[i + 1].y
		});
	}
	linesArray.push({
		xa: polygon[polygon.length - 1].x,
		ya: polygon[polygon.length - 1].y,
		xb: polygon[0].x,
		yb: polygon[0].y
	});
	return linesArray;
}

function defineLinesForNoFlyAreas(listOfNoflyPolygons) {
	var nestedListOfNoflyPolygonsLines = listOfNoflyPolygons.map(function (noFlyPolygon) {
		return getLinesForAreaPolygonFromPoints(noFlyPolygon);
	});
	return nestedListOfNoflyPolygonsLines.flat();
}

function isInsideOfNoFlyArea(noflyAreas, point) {
	var isPointInNOflyArea = noflyAreas.find(function (noFlyArea) {
		return isInside(noFlyArea, noFlyArea.length, point);
	});
	return !!isPointInNOflyArea;
}

module.exports = isPointSafe;
function isPointSafe(safeAreaPolygon, listOfNoflyPolygons, homePoint, safePoints) {
	var newSafePoints = safePoints ? safePoints : [];
	var isPointToHomePointCollidingWithSafeAreaPolygon;
	var isPointToHomePointCollidingWithNoFlyPolygon;
	let safeAreaPolygonLines = getLinesForAreaPolygonFromPoints(safeAreaPolygon);
	let noFlyPolygonsLines = defineLinesForNoFlyAreas(listOfNoflyPolygons);
	let yMax = Math.max.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.y;
		})
	);
	let xMax = Math.max.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.x;
		})
	);
	let yMin = Math.min.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.y;
		})
	);
	let xMin = Math.min.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.x;
		})
	);

	for (let x = xMin; x < xMax; x++) {
		for (let y = yMin; y < yMax; y++) {
			var pointToCheck = new Point(x, y);
			var pointIsOnlineSafePolygon = false;
			var pointIsOnlineNoFlyPolygon = false;
			//check if point is not on a polygon line
			for (var i = 0; i < safeAreaPolygonLines.length; i++) {
				if (
					pointIsOnLine(
						safeAreaPolygonLines[i].xa,
						safeAreaPolygonLines[i].ya,
						safeAreaPolygonLines[i].xb,
						safeAreaPolygonLines[i].yb,
						pointToCheck.x,
						pointToCheck.y
					)
				) {
					pointIsOnlineSafePolygon = true;
					break;
				}
			}

			for (var i; i < noFlyPolygonsLines.length; i++) {
				if (
					pointIsOnLine(
						noFlyPolygonsLines[i].xa,
						noFlyPolygonsLines[i].ya,
						noFlyPolygonsLines[i].xb,
						noFlyPolygonsLines[i].yb,
						pointToCheck.x,
						pointToCheck.y
					)
				) {
					pointIsOnlineNoFlyPolygon = true;
					break;
				}
			}

			//check if point is inside of a polygon and not inside of noflyarea
			if (
				!pointIsOnlineSafePolygon &&
				!pointIsOnlineNoFlyPolygon &&
				isInside(safeAreaPolygon, safeAreaPolygon.length, pointToCheck) &&
				!isInsideOfNoFlyArea(listOfNoflyPolygons, pointToCheck)
			) {
				for (let i = 0; i < safeAreaPolygonLines.length; i++) {
					isPointToHomePointCollidingWithSafeAreaPolygon = lineLine(
						pointToCheck.x,
						pointToCheck.y,
						homePoint.x,
						homePoint.y,
						safeAreaPolygonLines[i].xa,
						safeAreaPolygonLines[i].ya,
						safeAreaPolygonLines[i].xb,
						safeAreaPolygonLines[i].yb
					);
					if (isPointToHomePointCollidingWithSafeAreaPolygon) {
						break;
					}
				}
				//check if line pointToCheck and homePoint intersects noFlyPolygon
				for (let i = 0; i < noFlyPolygonsLines.length; i++) {
					isPointToHomePointCollidingWithNoFlyPolygon = lineLine(
						pointToCheck.x,
						pointToCheck.y,
						homePoint.x,
						homePoint.y,
						noFlyPolygonsLines[i].xa,
						noFlyPolygonsLines[i].ya,
						noFlyPolygonsLines[i].xb,
						noFlyPolygonsLines[i].yb
					);
					if (isPointToHomePointCollidingWithNoFlyPolygon) {
						break;
					}
				}
				//when we have already safepoints defined
				if (newSafePoints.length) {
					for (var i; i < safeAreaPolygonLines.length; i++) {
						for (var j; j < newSafePoints.length; j++) {
							isPointToSafePointCollidingWithSafeAreaPolygon = lineLine(
								pointToCheck.x,
								pointToCheck.y,
								newSafePoints[j].x,
								newSafePoints[j].y,
								safeAreaPolygonLines[i].xa,
								safeAreaPolygonLines[i].ya,
								safeAreaPolygonLines[i].xb,
								safeAreaPolygonLines[i].yb
							);
							if (isPointToSafePointCollidingWithSafeAreaPolygon) {
								break;
							}
						}
					}
				}

				if (isPointToHomePointCollidingWithSafeAreaPolygon || isPointToHomePointCollidingWithNoFlyPolygon) {
					newSafePoints.push(pointToCheck);
				}
			}
		}
	}
	attachNumberOfPointVisibleToNewSafePoint(safeAreaPolygon, listOfNoflyPolygons, newSafePoints);
}

// isPointSafe(
// 	[
// 		{
// 			x: 1,
// 			y: 1
// 		},
// 		{
// 			x: 13,
// 			y: 1
// 		},
// 		{
// 			x: 13,
// 			y: 13
// 		},
// 		{
// 			x: 1,
// 			y: 13
// 		}
// 	],
// 	[
// 		[
// 			{
// 				x: 3,
// 				y: 5
// 			},
// 			{
// 				x: 11,
// 				y: 5
// 			},
// 			{
// 				x: 11,
// 				y: 9
// 			},
// 			{
// 				x: 3,
// 				y: 9
// 			}
// 		]
// 	],
// 	{
// 		x: 7,
// 		y: 3
// 	}
// );

function attachNumberOfPointVisibleToNewSafePoint(safeAreaPolygon, noflyAreas, newSafePoints) {
	newSafePoints.map((point, index) => {
		point.numberOfConnections = findNumberOfPointsVisibleToSafePoint(safeAreaPolygon, noflyAreas, point);
	});
	findTheMaxValueAndThePoint(newSafePoints);
}

function findTheMaxValueAndThePoint(newSafePoints) {
	let maxPoint = newSafePoints.reduce((max, point) =>
		max.numberOfConnections > point.numberOfConnections ? max : point
	);
	console.log(maxPoint);
}

// iterate over every point defined with values of x, y
// between min, and max on each of the axis
function findNumberOfPointsVisibleToSafePoint(safeAreaPolygon, noflyAreas, safePoint) {
	var safeAreaPolygonLines = getLinesForAreaPolygonFromPoints(safeAreaPolygon);
	var noFlyPolygonsLines = defineLinesForNoFlyAreas(noflyAreas);
	var newlyCreatedSafePoint = safePoint;
	var numberOfSafeConnections = 0;
	var isPointToHomePointCollidingWithSafeAreaPolygonEven;
	var isPointToNewSafePointCollidingWithSafeAreaPolygon;
	var yMax = Math.max.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.y;
		})
	);
	var xMax = Math.max.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.x;
		})
	);
	var yMin = Math.min.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.y;
		})
	);
	var xMin = Math.min.apply(
		Math,
		safeAreaPolygon.map(function (point) {
			return point.x;
		})
	);
	for (let x = xMin; x < xMax; x++) {
		for (let y = yMin; y < yMax; y++) {
			let pointToCheckRayToSafePoint = new Point(x, y);

			var pointIsOnlineSafePolygon = false;
			var pointIsOnlineNoFlyPolygon = false;
			if (pointToCheckRayToSafePoint == newlyCreatedSafePoint) {
				break;
			}

			for (var i = 0; i < safeAreaPolygonLines.length; i++) {
				if (
					pointIsOnLine(
						safeAreaPolygonLines[i].xa,
						safeAreaPolygonLines[i].ya,
						safeAreaPolygonLines[i].xb,
						safeAreaPolygonLines[i].yb,
						pointToCheckRayToSafePoint.x,
						pointToCheckRayToSafePoint.y
					)
				) {
					pointIsOnlineSafePolygon = true;
					break;
				}
			}

			for (var i; i < noFlyPolygonsLines.length; i++) {
				if (
					pointIsOnLine(
						noFlyPolygonsLines[i].xa,
						noFlyPolygonsLines[i].ya,
						noFlyPolygonsLines[i].xb,
						noFlyPolygonsLines[i].yb,
						pointToCheckRayToSafePoint.x,
						pointToCheckRayToSafePoint.y
					)
				) {
					pointIsOnlineNoFlyPolygon = true;
					break;
				}
			}
			//check if point is inside
			if (
				!pointIsOnlineSafePolygon &&
				!pointIsOnlineNoFlyPolygon &&
				isInside(safeAreaPolygon, safeAreaPolygon.length, pointToCheckRayToSafePoint) &&
				!isInsideOfNoFlyArea(noflyAreas, pointToCheckRayToSafePoint)
			) {
				//check if this point is safe for homePoint
				//check if line pointToCheck and homePoint intersects safeAreaPolygon
				let isPointToNewSafePointCollidingWithSafeAreaPolygon;
				let isPointToNewSafePointCollidingWithWithNoFlyPolygon;
				var numberOfIntersectionsSafeArea = 0;
				for (var i = 0; i < safeAreaPolygonLines.length; i++) {
					isPointToNewSafePointCollidingWithSafeAreaPolygon = lineLine(
						pointToCheckRayToSafePoint.x,
						pointToCheckRayToSafePoint.y,
						newlyCreatedSafePoint.x,
						newlyCreatedSafePoint.y,
						safeAreaPolygonLines[i].xa,
						safeAreaPolygonLines[i].ya,
						safeAreaPolygonLines[i].xb,
						safeAreaPolygonLines[i].yb
					);
					if (isPointToNewSafePointCollidingWithSafeAreaPolygon) {
						break;
					}
				}

				for (var i = 0; i < noFlyPolygonsLines.length; i++) {
					isPointToNewSafePointCollidingWithWithNoFlyPolygon = lineLine(
						pointToCheckRayToSafePoint.x,
						pointToCheckRayToSafePoint.y,
						newlyCreatedSafePoint.x,
						newlyCreatedSafePoint.y,
						noFlyPolygonsLines[i].xa,
						noFlyPolygonsLines[i].ya,
						noFlyPolygonsLines[i].xb,
						noFlyPolygonsLines[i].yb
					);
					if (isPointToNewSafePointCollidingWithWithNoFlyPolygon) {
						if ((newlyCreatedSafePoint.x == 4, newlyCreatedSafePoint.y == 10)) {
						}
						break;
					}
				}
				if (
					!isPointToNewSafePointCollidingWithSafeAreaPolygon &&
					!isPointToNewSafePointCollidingWithWithNoFlyPolygon &&
					!pointIsOnlineSafePolygon &&
					!pointIsOnlineNoFlyPolygon
				) {
					numberOfSafeConnections = numberOfSafeConnections + 1;
				}
			}
		}
	}
	return numberOfSafeConnections;
}
